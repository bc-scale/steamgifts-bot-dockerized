import { AxiosRequestConfig, AxiosResponse } from "axios";
import cheerio from "cheerio";
import { Website } from "./Website";

export class SteamgiftsBot {
  website = new Website();
  xsrf_token: string = "";
  points: number = 0;
  headers = { Cookie: "PHPSESSID=" };
  html: string = "";
  minRandom: number;
  maxRandom: number;

  constructor(sessionId: string, minRandom: number, maxRandom: number, priorityPages: string[]) {
    this.headers.Cookie += sessionId;
    this.minRandom = minRandom;
    this.maxRandom = maxRandom;
    this.getTarget(priorityPages);
  }

  async getTarget(priorityPages: string[]): Promise<void> {
    let result: boolean = true;
    let currentPageNumber: number = 1;

    this.writeLog("Priority: [" + String(priorityPages) + "]");

    for (let i = 0; i < priorityPages.length; i++) {
      result = await this.getGames(priorityPages[i]);
      if (!result) {
        break;
      }
    }

    while (result === true) {
      result = await this.getGames("page=" + currentPageNumber);
      currentPageNumber++;
    }

    let random: number = this.getRndInteger(this.minRandom,this.maxRandom);
    this.writeLog("Waiting " + random + " Hours, to get more Points...");
    await this.wait(60 * 60 * random);
    this.getTarget(priorityPages);
    return;
  }

  async getGames(currentPage: string): Promise<boolean> {
    try {
      await this.getPage("https://www.steamgifts.com/giveaways/search?" + currentPage);

      // Extracting information from HTML

      const $ = cheerio.load(this.html);
      const gameList = $(".giveaway__row-inner-wrap");
      this.xsrf_token = String($('[name="xsrf_token"]').val());

      var tempPoints: string = String($(".nav__points").text());
      this.writeLog("Current Points:" + tempPoints);
      if (tempPoints) {
        this.points = Number(tempPoints);
      }
      else {
        throw new Error("Cant read points left, is your session valid?");
      }

      this.writeLog("Processing games from '" + currentPage + "'");

      for (let game of gameList) {
        const gameCost: number = Number(
          $(game)
            .find(".giveaway__heading__thin")
            .last()
            .text()
            .replace(/[^0-9]/g, "")
        );

        const gameName: string = $(game)
          .find(".giveaway__heading__name")
          .text();

        var gameCode: string = "";
        var tempGameCode = $(game).find(".giveaway__heading__name").attr("href");
        if (tempGameCode) {
          gameCode = tempGameCode.split("/")[2];
        }

        const isEntered: boolean = $(game).hasClass("is-faded");

        // Logic for entering giveaways

        if (this.points - gameCost < 0 && !isEntered) {
          this.writeLog(this.points + "P/" + gameCost + "P | Not enough Points to enter the next giveaway: \"" + gameName + "\".");

          // Not enough points left
          return false;
        } else if (!isEntered) {
          await this.wait(2); // 2 sec (request limit)

          await this.enterGiveaway("https://www.steamgifts.com/ajax.php", gameCode, gameName, gameCost);
          //this.writeLog("Would enter " + gameName + " for " + gameCost + " Points");
        }
      }

      return true;
    } catch (error) {
      throw error;
    }
  }

  async getPage(websiteUrl: string): Promise<AxiosResponse> {
    try {
      const config: AxiosRequestConfig = {
        url: websiteUrl,
        method: "get",
        headers: this.headers,
      };

      return await this.website
        .getPage(config)
        .then((response: AxiosResponse) => {
          if (response.status === 200) {
            this.html = response.data;

            return response;
          } else {
            throw console.error(response.status + " - " + response.statusText);
          }
        });
    } catch (error) {
      throw error;
    }
  }

  async enterGiveaway(
    url: string,
    gameCode: string,
    gameName: string,
    gameCost: number
  ): Promise<AxiosResponse> {
    try {
      const payload = `xsrf_token=${this.xsrf_token}&do=entry_insert&code=${gameCode}`;

      const config: AxiosRequestConfig = {
        url: url,
        method: "post",
        headers: this.headers,
        data: payload,
      };

      return this.website.getPage(config).then((response: AxiosResponse) => {
        if (response.status === 200) {
          this.writeLog(this.points + "P/" + gameCost + "P | Entering giveaway: " + gameName);
          this.points = this.points - gameCost;

          return response;
        } else {
          throw new Error("HTTP STATUS: " + response.status + " - " + response.statusText);
        }
      });
    } catch (error) {
      throw error;
    }
  }

  writeLog(text: string): void {
    const log =
      new Date().toLocaleString(undefined, {
        day: "2-digit",
        month: "2-digit",
        year: "numeric",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit", // All of this options to allways have same time log length
      }) +
      " - " +
      text;
    console.log(log);
  }

  async wait(time: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, 1000 * time)); // Time in seconds
  }

  getRndInteger(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min)) + min;
  }
}
