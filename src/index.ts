import { SteamgiftsBot } from "./steamgiftsBot";
const args = process.argv.slice(2);
let sessionId: string = args[0];
let minRandom: number = Number(args[1]);
let maxRandom: number = Number(args[2]);
let priorityPagesString: string = (args[3]) ? args[3] : "";

let priorityPages: string[] = (priorityPagesString !== "") ? priorityPagesString.split(",") : [];

new SteamgiftsBot(sessionId, minRandom, maxRandom, priorityPages);
