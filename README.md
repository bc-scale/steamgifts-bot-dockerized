# Dockerized - Steamgifts-bot
GitLab: [https://gitlab.com/docker_repos/steamgifts-bot-dockerized](https://gitlab.com/docker_repos/steamgifts-bot-dockerized)  
DockerHub: [https://hub.docker.com/r/mega349/steamgifts-bot-dockerized](https://hub.docker.com/r/mega349/steamgifts-bot-dockerized)    
Fork from: [butilka123/steamgifts-bot](https://github.com/butilka123/steamgifts-bot) [SHA:[49f6e3fc18e49f35fbc052994411ac1a77d38f1e](https://github.com/butilka123/steamgifts-bot/tree/49f6e3fc18e49f35fbc052994411ac1a77d38f1e)]

## Description
Autojoin bot for [steamgifts.com](https://www.steamgifts.com) giveaways.  

The bot will enter every visible giveaway at [page 1](https://www.steamgifts.com/giveaways/search?page=1), if the page its empty the next page will be used.  
If you setup [Priority Pages](#priority-pages) they will be performed first in given order.  
If your points are empty, the bot will wait an random amount of hours (configurable) to farm points.

## Setup
1. Log on at [steamgifts.com](https://www.steamgifts.com)
2. **Hide what you cant enter at your [giveaway settings](https://www.steamgifts.com/account/settings/giveaways)!!!**
3. Browse your cookies (maybe you will need an Browser-Addon) and copy the SessionID
4. Setup your [Compose file](#docker-compose-file) and fill the sessionID
5. **DONT** Log out or your session will be invalidated (you **can** close the window or page)

## Docker Compose file
The Example Compose file is for Docker Swarm mode, change it by yourself for standalone.  
*If your session is invalid the bot will crash (intended), setup a restart policy!*
```yml
version: '3.8'

services:
  app:
    image: mega349/steamgifts-bot-dockerized:latest
    deploy:
      restart_policy:
        condition: on-failure
        delay: 5s
        max_attempts: 3
        window: 120s
    environment:
      - TZ=Europe/Berlin
      - SESSIONID=sessionIdHere 
      - PRIORITYPAGES=copy_min=2,type=group,type=wishlist,dlc=true # Example
      - MINRANDOM=1
      - MAXRANDOM=15
```

## Environment vars
|ENV|Description|Info|default value|NEEDED|
|-|-|-|-|-|
|TZ|Your Timezone||Europe/Amsterdam|No|
|SESSIONID|SessionID from cookie||00000|**Yes**|
|PRIORITYPAGES|Will be performed first in given order|"," - Seperated||No|
|MINRANDOM|Min hours to wait for points|0 or more|1|No|
|MAXRANDOM|Max hours to wait for points|even or higher then MINVALUE|15|No|

## Priority Pages
The part of the URL after `https://www.steamgifts.com/giveaways/search?` 
|Page|Description|Example|Info|
|-|-|-|-|
|copy_min=#|Multiple Copies|copy_min=2|Replace # with min. amount of copies|
|type=group|Group|type=group||
|type=wishlist|Wishlist|type=wishlist||
|dlc=true|DLC|dlc=true||
|type=recommended|Recommended|type=recommended||
|type=new|New|type=new||
|q=#|Search Query|q=epic%20survivors| Use `%20` instead of Space|
|page=#|All|page=12|just needed if you want enter page # befor page 1|
