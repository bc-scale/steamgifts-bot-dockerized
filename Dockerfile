FROM node:18

ARG DEBIAN_FRONTEND=noninteractive

ENV TZ=Europe/Amsterdam
ENV SESSIONID=00000
ENV PRIORITYPAGES=
ENV MINRANDOM=1
ENV MAXRANDOM=15

RUN mkdir /app
COPY ./app/ /app/
RUN mkdir /app/storage
RUN chown -R node:node /app


RUN apt update
RUN apt install tzdata -y

USER node
WORKDIR /app
ENTRYPOINT ["sh", "./run.sh"]
